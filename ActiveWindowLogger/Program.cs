﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CsvHelper;
using System.IO;

namespace ActiveWindowLogger
{
    class ActiveWindowRecord
    {
        public string ComputerName { get; set; }
        public DateTime StartDateTime { get; set; }
        public ulong StartUnixTimeMilliseconds { get; set; }
        public ulong DurationInMilliseconds { get; set; }
        public string ProcessFilePath { get; set; }
        public string ProcessName { get; set; }
        public string WindowTitle { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string filename = "ActiveWindowLog_" + Guid.NewGuid() + ".csv";
            Console.WriteLine(Path.GetFullPath(filename));

            writer = new StreamWriter(filename);
            writer.AutoFlush = true;
            csv = new CsvWriter(writer);

            csv.WriteHeader<ActiveWindowRecord>();
            csv.NextRecord();

            activeWindow = new ActiveWindow();
            activeWindow.ActiveWindowChanging += ActiveWindow_ActiveWindowChanging;

            Application.Run();
        }

        private static void ActiveWindow_ActiveWindowChanging(object sender, EventArgs e)
        {
            var expiringActiveWindow = sender as ActiveWindow;
            if (expiringActiveWindow != null)
            {
                var process = expiringActiveWindow.Process;
                if (process != null)
                {
                    csv.WriteRecord(new ActiveWindowRecord
                    {
                        ComputerName = computerName,
                        StartDateTime = activeWindowStartDateTime,
                        StartUnixTimeMilliseconds = (ulong)((DateTimeOffset)activeWindowStartDateTime).ToUnixTimeMilliseconds(),
                        DurationInMilliseconds = (ulong)Math.Round((DateTime.Now - activeWindowStartDateTime).TotalMilliseconds),
                        ProcessFilePath = process.MainModule.FileName,
                        ProcessName = process.MainModule.ModuleName,
                        WindowTitle = process.MainWindowTitle
                    });
                    csv.NextRecord();
                    Console.WriteLine("Active window record logged: " + process.MainModule.ModuleName);
                }
            }

            activeWindowStartDateTime = DateTime.Now;
        }

        private static ActiveWindow activeWindow;
        private static DateTime activeWindowStartDateTime;

        private static StreamWriter writer;
        private static CsvWriter csv;

        private static readonly string computerName = Environment.MachineName;
    }
}
