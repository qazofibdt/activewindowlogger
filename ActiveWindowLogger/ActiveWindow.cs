﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace ActiveWindowLogger
{
    class User32
    {
        public delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);

        [DllImport("user32.dll")]
        public static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, WinEventDelegate lpfnWinEventProc, uint idProcess, uint idThread, uint dwFlags);

        public const uint WINEVENT_OUTOFCONTEXT = 0;
        public const uint EVENT_SYSTEM_FOREGROUND = 3;

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
    }

    class ActiveWindow
    {
        public ActiveWindow()
        {
            outOfContextHandler = new User32.WinEventDelegate(OutOfContextHandler);
            IntPtr m_hhook = User32.SetWinEventHook(User32.EVENT_SYSTEM_FOREGROUND, User32.EVENT_SYSTEM_FOREGROUND, IntPtr.Zero, outOfContextHandler, 0, 0, User32.WINEVENT_OUTOFCONTEXT);
        }

        public event EventHandler ActiveWindowChanging;

        public Process Process
        {
            get
            {
                return process;
            }
        }

        private void OnActiveWindowChanging()
        {
            ActiveWindowChanging?.Invoke(this, new EventArgs());
        }

        private void OutOfContextHandler(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime) //STATIC
        {
            OnActiveWindowChanging();
            activeWindowHandle = User32.GetForegroundWindow();
            uint processId = 0;
            uint threadId = User32.GetWindowThreadProcessId(activeWindowHandle, out processId);
            process = Process.GetProcessById((int)processId);
        }

        private readonly User32.WinEventDelegate outOfContextHandler = null;

        private IntPtr activeWindowHandle;
        private Process process;
    }
}
